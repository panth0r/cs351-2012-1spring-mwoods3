#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "poker.h"

/* converts a hand (of 5 cards) to a string representation, and stores it in the
 * provided buffer. The buffer is assumed to be large enough.
 */
void hand_to_string (hand_t hand, char *handstr) {
    char *p = handstr;
    int i;
    char *val, *suit;
    for (i=0; i<5; i++) {
        if (hand[i].value < 10) {
            *p++ = hand[i].value + '0';
        } else {
            switch(hand[i].value) {
            case 10: *p++ = 'T'; break;
            case 11: *p++ = 'J'; break;
            case 12: *p++ = 'Q'; break;
            case 13: *p++ = 'K'; break;
            case 14: *p++ = 'A'; break;
            }
        }
        switch(hand[i].suit) {
        case DIAMOND: *p++ = 'D'; break;
        case CLUB: *p++ = 'C'; break;
        case HEART: *p++ = 'H'; break;
        case SPADE: *p++ = 'S'; break;
        }
        if (i<=3) *p++ = ' ';
    }
    *p = '\0';
}

/* converts a string representation of a hand into 5 separate card structs. The
 * given array of cards is populated with the card values.
 */
void string_to_hand (const char *handstr, hand_t hand) {
    char *p = (char *)handstr;
    int i = 0;
    int j = 0;
    /* if i could have imagined a worse way to write this,
     * that'd probably be what's here */
    for(i=0;i<5;i++) {
	for(j=0;j<3;j++) {
	    switch(handstr[(3*i)+j]) {
                case '2': hand[i].value = 2; break;
	        case '3': hand[i].value = 3; break;
	        case '4': hand[i].value = 4; break;
	        case '5': hand[i].value = 5; break;
	        case '6': hand[i].value = 6; break;
	        case '7': hand[i].value = 7; break;
	        case '8': hand[i].value = 8; break;
	        case '9': hand[i].value = 9; break;
	        case 'T': hand[i].value = 10; break;
	        case 'J': hand[i].value = 11; break;
	        case 'Q': hand[i].value = 12; break;
	        case 'K': hand[i].value = 13; break;
	        case 'A': hand[i].value = 14; break;
	        case 'D': hand[i].suit = DIAMOND; break;
	        case 'C': hand[i].suit = CLUB; break;
	        case 'H': hand[i].suit = HEART; break;
	        case 'S': hand[i].suit = SPADE; break;
	    }
	}
    }
}

/* swaps the contents of card_a with that of card_b */
void swap_cards (card_t *card_a, card_t *card_b){
    card_t tmp = *card_a;
    *card_a = *card_b;
    *card_b = tmp;
}

/* sorts the hands so that the cards are in ascending order of value (two
 * lowest, ace highest */
void sort_hand (hand_t hand) {
    int i;
    int j;
    for(i=0;i<5;i++) {
	for(j=i;j<5;j++) {
	    if(hand[i].value > hand[j].value) {
		swap_cards(&hand[i], &hand[j]);
	    }
	}
    }
}

/* counts the number of pairs in a given hand */
int count_pairs (hand_t hand) {
    sort_hand(hand);
    int pairs = 0;
    if(hand[0].value == hand[1].value) {
	pairs++;
	if(hand[2].value == hand[3].value) {
	    pairs++;
	}
	if(hand[3].value == hand[4].value) {
	    pairs++;
	}
	return pairs;
    }
    if(hand[1].value == hand[2].value) {
	pairs++;
	if(hand[3].value == hand[4].value) {
	    pairs++;
	}
	return pairs;
    }
    if(hand[3].value == hand[4].value) {
	pairs++;
	return pairs;
    }
    if(hand[2].value == hand[3].value) {
	pairs++;
	return pairs;
    }
    return pairs;
}

int is_onepair (hand_t hand) {	//2
    if(count_pairs(hand) >= 1) {
	return 1;
    }
    return 0;
}

int is_twopairs (hand_t hand) {	//3
    if(count_pairs(hand) == 2) {
	return 1;
    }
    return 0;
}

/* counts the number of cards in a given hand,
 * returns number of like cards once goal is
 * reached or returns 0 if goal not reached.
 * implemented by is_threeofakind & is_fourofakind
 */
int ofakind(int goal, hand_t hand) {
    int kind = 1;
    int i;
    int j;
    sort_hand(hand);
    for(i=0;i<5;i++) {
	for(j=i+1;j<5;j++) {
            if(hand[i].value == hand[j].value) {
		kind = kind++;
		if(kind >= goal) {
		    return kind;
		}
	    } else {
	        kind = 1;
	    }
	}
    }
    return 0;
}

int is_threeofakind (hand_t hand) {	//4
    if(ofakind(3,hand)) {
	return 1;
    }
    return 0;
}

int is_straight (hand_t hand) {	//5
    int i = 0;
    sort_hand(hand);
    if(hand[0].value == 2 && hand[1].value == 3 && hand[2].value == 4 && hand[3].value == 5 && hand[4].value==14) {
	return 1;
    } else {
	while(i<4) {
            if(hand[i].value == hand[i+1].value - 1) {
	        i++;
	        if(i==4) {
		    return 1;
	        }
	    } else {
	        return 0;
	    }
        }
        return 0;
    }
}

int is_fullhouse (hand_t hand) {	//6
    sort_hand(hand);
    if(hand[0].value == hand[1].value && hand[3].value == hand[4].value &&
      (hand[1].value == hand[2].value || hand[2].value == hand[3].value )) {
	return 1;
    } else {
	return 0;
    }
}

int is_flush (hand_t hand) {	//7
    sort_hand(hand);
    int i;
    for(i=0;i<4;i++){
        if(hand[i].suit != hand[i+1].suit) {
	    return 0;
	}
    }
    return 1;
}

int is_straightflush (hand_t hand) {	//8
    if(is_straight(hand) && is_flush(hand)) {
	return 1;
    } else {
        return 0;
    }
}

int is_fourofakind (hand_t hand) {	//9
    if(ofakind(4,hand)) {
	return 1;
    }
    return 0;
}

int is_royalflush (hand_t hand) {	//10
    if(is_straight(hand) && is_flush(hand) && hand[0].value==10 && hand[4].value ==14) {
	return 1;
    }
    return 0;
}

/* returns numeric representation of rank,
 * with the strongest hand (a royal flush)
 * having the largest number (10)
 */
int rank(hand_t hand) {
    if(is_royalflush(hand)) return 10;
    if(is_fourofakind(hand)) return 9;
    if(is_straightflush(hand)) return 8;
    if(is_flush(hand)) return 7;
    if(is_fullhouse(hand)) return 6;
    if(is_straight(hand)) return 5;
    if(is_threeofakind(hand)) return 4;
    if(is_twopairs(hand)) return 3;
    if(is_onepair(hand)) return 2;
    return 1;
}

/* compares the hands based on rank -- if the ranks (and rank values) are
 * identical, compares the hands based on their highcards.
 * returns 0 if h1 > h2, 1 if h2 > h1.
 */
int compare_hands (hand_t h1, hand_t h2) {
    if(rank(h1) > rank(h2)) return 0;
    if(rank(h1) < rank(h2)) return 1;
    if(rank(h1) == rank(h2)) {
	if(get_value_back(h1) > get_value_back(h2)) return 0;
	if(get_value_back(h1) < get_value_back(h2)) return 1;
	if(get_value_back(h1) == get_value_back(h2)) {
	    if(get_value_forward(h1) > get_value_forward(h2)) return 0;
	    if(get_value_forward(h1) < get_value_forward(h2)) return 1;
	}
    }
    return compare_highcards(h1, h2);
}

/* for hands such as twopairs and fullhouse, returns the value of
 * the largest pair
 * implemented by comare_hands
 */
int get_value_back(hand_t hand) {
    sort_hand(hand);
    int i;
    for(i=4;i>0;i--) {
	if(hand[i].value == hand[i-1].value) {
	    return hand[i].value;
	}
    }
    return 0;
}

/* like above, but returns the value of the smallest pair */
int get_value_forward(hand_t hand) {
    sort_hand(hand);
    int i;
    for(i=0;i<4;i++) {
	if(hand[i].value == hand[i+1].value) {
	    return hand[i].value;
	}
    }
}

/* compares the hands based solely on their highcard values (ignoring rank). if
 * the highcards are a draw, compare the next set of highcards, and so forth.
 */
int compare_highcards (hand_t h1, hand_t h2) {	//1
    sort_hand(h1);
    sort_hand(h2);
    int i;
    for(i=4;i>-1;i--) {
	if(h1[i].value > h2[i].value) {
	    return 0;
	}
	if(h1[i].value < h2[i].value) {
	    return 1;
	}
    }
    return 0;
}
