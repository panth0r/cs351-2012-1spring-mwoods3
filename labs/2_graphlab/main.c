#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

int main (int argc, char *argv[]) {
	int i;
	vertex_t *vlist_head = NULL, *vp;
//	vertex_t *tour_head;
	adj_vertex_t *adj_v;

	if (argc == 1 || (argc-1)%3 != 0) {
		return 0;
	}
	for (i=1; i<argc; i+=3) {
		add_edge(&vlist_head, argv[i], argv[i+1], atoi(argv[i+2]));
	}
	
	int number_of_vertexes = 0;
	printf("Adjacency list:\n");
	for (vp = vlist_head; vp != NULL; vp = vp->next) {
		printf("  %s: ", vp->name);
		number_of_vertexes++;
		for (adj_v = vp->adj_list; adj_v != NULL; adj_v = adj_v->next) {
			printf("%s(%d) ", adj_v->vertex->name, adj_v->edge_weight);
		}
		printf("\n");
	}

	printf("\nPossible tour:\n");
	
	printf("\n");

	if (vlist_head != NULL) {
		free_vertex_list(vlist_head);
		vlist_head = NULL;
	}
	return 0;
}
