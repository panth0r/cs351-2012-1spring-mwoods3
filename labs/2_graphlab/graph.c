#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph.h"

/* implement the functions you declare in graph.h here */

vertex_t* create_vertex(vertex_t **vtxhead, char *name) {
	if (*vtxhead != NULL) {
		return create_vertex(&((*vtxhead)->next), name);
	} else {
		vertex_t *vtx = malloc(sizeof(vertex_t));
		vtx->name = name;
		vtx->adj_list = NULL;
		vtx->next = NULL;
		*vtxhead = vtx;
		return vtx;
	}
}

adj_vertex_t* create_adj_vertex(adj_vertex_t **adjvtxhead, vertex_t *vtx, int weight) {
	if (*adjvtxhead != NULL) {
		return create_adj_vertex(&((*adjvtxhead)->next), vtx, weight);
	} else {
		adj_vertex_t *adjvtx = malloc(sizeof(adj_vertex_t));
		adjvtx->vertex = vtx;
		adjvtx->edge_weight = weight;
		adjvtx->next = NULL;
		*adjvtxhead = adjvtx;
		return adjvtx;
	}
}

void free_adj_vertex_list(adj_vertex_t *adjvtx) {
	if (adjvtx->next != NULL) {
		free_adj_vertex_list(adjvtx->next);
		adjvtx->next = NULL;
	}
	free(adjvtx);
}
	

void free_vertex_list(vertex_t *vtx) {
	if (vtx->next != NULL) {
		free_vertex_list(vtx->next);
		vtx->next = NULL;
	}
	if (vtx->adj_list != NULL) free_adj_vertex_list(vtx->adj_list);
	free(vtx);
}

void link (vertex_t *v1, vertex_t *v2, int weight) {
	create_adj_vertex(&(v1->adj_list), v2, weight);
	create_adj_vertex(&(v2->adj_list), v1, weight);	
}

vertex_t *search_vertexes(vertex_t *vtxhead, char *v_name) {
	while(vtxhead != NULL) {
		if(strcmp(vtxhead->name, v_name) == 0) return vtxhead;
		else vtxhead = vtxhead->next;
	}
	return NULL;
}
/*
vertex_t **build_tour(vertex_t *vtx, vertex_t *tour, int index, int size) {
	printf("in build_tor; index=%d, size=%d\n", index, size);
	if(index == size) {
		return *tour;
	}
	adj_vertex_t *adj_v = vtx->adj_list;
	while(adj_v != NULL) {
		if(search_vertexes(tour, adj_v->vertex->name) == NULL) {
			create_adj_vertex(tour, adj_v->vertex, adj_v->weight);
			//tour[index] = adj_v->vertex;
			build_tour(adj_v->vertex, &tour, index+1, size);
		} else {
			adj_v = adj_v->next;
		}
	}
	if(vtx->next != NULL) build_tour(vtx->next, tour, 0, size);
	return NULL;
}
*/
/*vertex_t *build_tour(vertex_t *vtx, vertex_t **tour, int index, int size) {
	adj_vertex_t *adj_v;
	for(adj_v = vtx->adj_list; adj_v != NULL; adj_v = adj_v->next) {
		printf("***vtx=%s::adj_v=%s***",vtx->name,adj_v->vertex->name);
		printf("***index=%d::size=%d***",index,size);
		if(index == size-1) {
			printf("***index == size***");
			//this is the "size == index, so a tour was found" case
			return *tour;
		}
		if(search_vertexes(*tour, adj_v->vertex->name) == NULL) {
			printf("***search_vertexes == NULL***");
			//this is the "vertex is not in the tour, so add it" case
			//adds adj_v and vtx on on initial run to tour
			add_edge(tour, vtx->name, adj_v->vertex->name, adj_v->edge_weight);
			//sets adj_v to the current working vertex (after it's been pushed to the tour)
			vtx = adj_v->vertex;
			printf("***tour[%d]=%s***", index, tour[index]->name);
			printf("***calling %s, tour, %d, %d***",vtx->name, index, size);
			build_tour(vtx, tour, index+1, size);
		}
		if(adj_v->next == NULL) {
			printf("***adj_v->next == NULL***");
			if(vtx->next == NULL) {
				printf("\nNO PATH FOUND\n");
				exit(0);
				//this is the "no tour found" case
			} else {
				printf("\n***vtx->next was not NULL (index being reset)***\n");
				//this is the "move to the next vtx" case
				vtx = vtx->next;
				index = 0;
				build_tour(vtx, tour, index, size);
			}
			//iteration through the for loop means moving on to the next
			//	adj_v of the current vtx
		}
	}
	return NULL;
}*/

void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight) {	
	vertex_t *v1 = search_vertexes(*vtxhead, v1_name);
	vertex_t *v2 = search_vertexes(*vtxhead, v2_name);	
	if(v1 == NULL) v1 = create_vertex(vtxhead, v1_name);
	if(v2 == NULL) v2 = create_vertex(vtxhead, v2_name);
	link(v1, v2, weight);
}
