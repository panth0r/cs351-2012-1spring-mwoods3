/* 
 * hello.c - the quintessential hello world program
 */

#include <stdio.h>

typedef char * string;

int main ()
{
  string nonsense[3];
  nonsense[0] = "Michael Kory Woods";
  nonsense[1] = "A20064249";
  nonsense[2] = "I don't buy anybody programs with butterflies.";

  int n;
  for(n = 1; n < 4; n++)
  {
    printf("%d. %s\n", n, nonsense[n-1]);
  }
  return 0;
}
